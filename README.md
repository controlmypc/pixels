# pixels

Meta repository for CMPC Pixels bug tracking. Project is not yet free software.

If you find a bug, please open an issue at https://gitlab.com/controlmypc/pixels/-/issues/new.

- **https://www.twitch.tv/controlmypc**
- https://cmpc.live
- https://twitter.com/MarcelD505
